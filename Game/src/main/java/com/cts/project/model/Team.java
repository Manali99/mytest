package com.cts.project.model;

import java.util.List;

public class Team {
	private String teamName;
	private String teamCode;
	private List<Player> players;
	
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getTeamCode() {
		return teamCode;
	}
	public void setTeamCode(String teamCode) {
		this.teamCode = teamCode;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	
	public void display()
	{
		
		System.out.println("Team name : " + teamName);
		System.out.println("Team code : " + teamCode);
		System.out.println("=======Players========");
		for(Player player : players)
		{
			System.out.println(player);
		}
		
	}
	
}
