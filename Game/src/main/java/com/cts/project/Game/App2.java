package com.cts.project.Game;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cts.project.model.Player;
import com.cts.project.model.Team;

public class App2 
{
    public static void main( String[] args )
    {
    	/*
    	 * Dependency Check
    	 */
    	ApplicationContext ctx = new ClassPathXmlApplicationContext("configure2.xml");
    	
    	Player p1 = (Player)ctx.getBean("player");
    	Player p2 = (Player)ctx.getBean("player");
    	
    	p1.setPlayerName("Virat");
    	p1.setPlayerId(10);
    	p1.setPlayerSkill("Batsman");
    	p1.setPlayerCountry("India");
    	
    	p2.setPlayerName("Dhoni");
    	p2.setPlayerId(7);
    	p2.setPlayerSkill("Skipper");
    	p2.setPlayerCountry("India");
    	
    	System.out.println(p1);
    	System.out.println(p2);
    	
    }
}
