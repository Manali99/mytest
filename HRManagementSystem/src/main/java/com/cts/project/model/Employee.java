package com.cts.project.model;

public class Employee {
	private int empId;
	private String empName;
	private String cohortCode;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getCohortCode() {
		return cohortCode;
	}

	public void setCohortCode(String cohortCode) {
		this.cohortCode = cohortCode;
	}

	public void display() {
		System.out.println(empId);
		System.out.println(empName);
		System.out.println(cohortCode);
	}
}
