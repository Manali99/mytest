package com.cts.project.HRManagementSystem;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cts.project.model.Employee;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //System.out.println( "Hello World!" );
    	ApplicationContext ctx=new ClassPathXmlApplicationContext("config.xml");
        
    	/*
    	Employee emp=(Employee)ctx.getBean("emp1");
        emp.display();
        */
    	/*
    	Employee emp=(Employee)ctx.getBean("emp1","emp2");
        emp.display();
        */
    	
    	Employee emp=(Employee)ctx.getBean("emp1");
        emp.display();
        
        emp=(Employee)ctx.getBean("emp2");
        emp.display();
    }
}
